package com.example.demo.service.api;

import com.example.demo.dto.CardholderDto;


public interface CardholderService {

    CardholderDto getCardholderById(int id);

    CardholderDto addCardholder(CardholderDto cardholderDto);

    void deleteCardholder(int id);

    CardholderDto updateCardholder(CardholderDto cardholderDto);
}
