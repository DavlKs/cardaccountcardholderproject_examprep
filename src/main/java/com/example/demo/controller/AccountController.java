package com.example.demo.controller;

import com.example.demo.dto.AccountDto;
import com.example.demo.exceptionHandling.dto.AccountNotFoundDto;
import com.example.demo.exceptionHandling.dto.CardholderNotFoundDto;
import com.example.demo.exceptionHandling.exception.AccountNotFoundException;
import com.example.demo.exceptionHandling.exception.CardholderNotFoundException;
import com.example.demo.service.api.AccountService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/accounts")
@Tag(name = "Счет", description = "API для счетов")
public class AccountController {

    AccountService accountService;

    @Autowired
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @PostMapping
    @Operation(summary = "Добавление нового счета")
    public AccountDto addAccount(@RequestBody AccountDto accountDto) {
        return accountService.addAccount(accountDto);
    }


    @PutMapping("/block/{id}")
    @Operation(summary = "Блокирование счета")
    public AccountDto blockAccountById(@PathVariable int id) {
        return accountService.blockAccountById(id);
    }

    @PutMapping("/unblock/{id}")
    @Operation(summary = "Разлокирование счета")
    public AccountDto unblockAccountById(@PathVariable int id) {
        return accountService.unblockAccountById(id);
    }

    @GetMapping("/{id}")
    @Operation(summary = "Получение счета по идентификатору держателя счета")
    List<AccountDto> getAllAccountsByAccountHolderId(@PathVariable int id) {
        return accountService.getAllAccountsByAccountHolderId(id);
    }

    @ExceptionHandler
    public ResponseEntity<AccountNotFoundDto> handleException (AccountNotFoundException exception) {
        AccountNotFoundDto account = new AccountNotFoundDto();
        account.setInfo(exception.getMessage());
        return new ResponseEntity<>(account, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler
    public ResponseEntity<CardholderNotFoundDto> handleException (CardholderNotFoundException exception) {
        CardholderNotFoundDto cardholder = new CardholderNotFoundDto();
        cardholder.setInfo(exception.getMessage());
        return new ResponseEntity<>(cardholder, HttpStatus.NOT_FOUND);
    }

}
