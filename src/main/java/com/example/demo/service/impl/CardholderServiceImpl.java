package com.example.demo.service.impl;

import com.example.demo.dao.CardholderMapper;
import com.example.demo.dto.CardholderDto;
import com.example.demo.entity.Cardholder;
import com.example.demo.exceptionHandling.exception.CardholderNotFoundException;
import com.example.demo.service.api.CardholderService;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class CardholderServiceImpl  implements CardholderService {

    ModelMapper modelMapper;
    CardholderMapper cardholderMapper;

    @Autowired
    public CardholderServiceImpl(ModelMapper modelMapper, CardholderMapper cardholderMapper) {
        this.modelMapper = modelMapper;
        this.cardholderMapper = cardholderMapper;
    }

    @Override
    public CardholderDto getCardholderById(int id) {
        Cardholder cardholder = cardholderMapper.getCardholderById(id).orElseThrow(
                () -> new CardholderNotFoundException("There is no cardholder with id = " + id)
        );
        log.error("Cardholder with id = {} is found", id);
        return modelMapper.map(cardholder, CardholderDto.class);
    }

    @Override
    public CardholderDto addCardholder(CardholderDto cardholderDto) {
        Cardholder cardholder = modelMapper.map(cardholderDto, Cardholder.class);
        cardholderMapper.addCardholder(cardholder);
        log.info("Cardholder with id = {} id added to database", cardholder.getCardholderId());
        return modelMapper.map(cardholder, CardholderDto.class);
    }

    @Override
    public void deleteCardholder(int id) {
        cardholderMapper.getCardholderById(id)
                .orElseThrow(() -> new CardholderNotFoundException("There is no cardholder with id = " + id));
        cardholderMapper.deleteCardholderCards(id);
        cardholderMapper.deleteCardholderAccounts(id);
        cardholderMapper.deleteCardholder(id);
        log.info("Cardholder with id = {} is deleted", id);
    }

    @Override
    public CardholderDto updateCardholder(CardholderDto cardholderDto) {
        Cardholder cardholder = modelMapper.map(cardholderDto, Cardholder.class);
        cardholderMapper.updateCardholder(cardholder);
        log.debug("Cardholder ");
        return modelMapper.map(cardholder, CardholderDto.class);
    }
}