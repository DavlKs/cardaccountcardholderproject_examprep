package com.example.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Cardholder {

    int cardholderId;
    String firstName;
    String lastName;
    String patronymicName;
    LocalDate birthDate;
    String birthPlace;
    String passNumber;

}
