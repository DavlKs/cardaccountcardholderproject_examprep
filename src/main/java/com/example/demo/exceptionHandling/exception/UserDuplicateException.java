package com.example.demo.exceptionHandling.exception;

public class UserDuplicateException extends RuntimeException {

    public UserDuplicateException (String message) {
        super(message);
    }
}
