package com.example.demo.service.api;

import com.example.demo.dto.AccountDto;

import java.util.List;

public interface AccountService {

    AccountDto addAccount(AccountDto accountDto);

    AccountDto updateAccount(AccountDto accountDto);

    AccountDto unblockAccountById(int id);

    AccountDto blockAccountById(int id);

    List<AccountDto> getAllAccountsByAccountHolderId(int id);

    AccountDto getAccountById(int id);
}
