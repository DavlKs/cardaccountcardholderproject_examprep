package com.example.demo.server;

import com.example.CardServerGrpc;
import com.example.Service;
import com.example.demo.dao.BusinessCardMapper;
import com.example.demo.dto.BusinessCardDto;
import com.example.demo.entity.BusinessCard;
import com.example.demo.service.api.BusinessCardService;
import com.example.demo.service.impl.BusinessCardServiceImpl;
import io.grpc.stub.StreamObserver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GRPCServer extends CardServerGrpc.CardServerImplBase  {

    @Autowired
    private BusinessCardServiceImpl businessCardService;


    public void getCardNumber(Service.GetCardNumberRequest request,
                              StreamObserver<Service.GetCardNumberResponse> responseObserver) {

        System.out.println(request);

        BusinessCardDto businessCard = businessCardService.getBusinessCardById(request.getCardId());

        System.out.println(businessCard.getCardNumber());

        Service.GetCardNumberResponse getCardNumberResponse = Service.GetCardNumberResponse
                .newBuilder()
                .setCardNumber(businessCard.getCardNumber())
                .build();

        responseObserver.onNext(getCardNumberResponse);

        responseObserver.onCompleted();


    }

}
