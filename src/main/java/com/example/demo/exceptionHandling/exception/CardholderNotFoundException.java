package com.example.demo.exceptionHandling.exception;

public class CardholderNotFoundException extends RuntimeException {

    public CardholderNotFoundException(String message) {
        super(message);
    }
}
