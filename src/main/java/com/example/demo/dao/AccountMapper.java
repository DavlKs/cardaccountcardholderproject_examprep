package com.example.demo.dao;

import com.example.demo.entity.Account;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Mapper
@Repository
public interface AccountMapper {

    @Insert("insert into accounts values (#{accountId}, #{accountNumber}, #{accountHolder}, " +
            "#{isBlocked})")
    @SelectKey(keyProperty = "accountId", before = true, resultType = Integer.class,
            statement = "select nextval('account_id_seq')")
    void addAccount(Account account);

    @Update("update accounts set account_number = #{accountNumber}, account_holder = #{accountHolder}," +
            "is_blocked = #{isBlocked} where account_id = #{accountId}")
    void updateAccount(Account account);

    @Update("update accounts set is_blocked = true where account_id = #{id}")
    void blockAccountById(int id);

    @Update("update accounts set is_blocked = false where account_id = #{id}")
    void unblockAccountById(int id);

    @Select("select * from accounts where account_holder = #{id}")
    List<Account> getAllAccountsByAccountHolderId(int id);

    @Select("select * from accounts where account_id = #{id}")
    Optional<Account> getAccountById(int id);
}
