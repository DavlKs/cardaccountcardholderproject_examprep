
create sequence if not exists cardholder_id_seq start 1;

CREATE TABLE cardholders (
                             cardholder_id INTEGER not null default nextval('cardholder_id_seq') primary key,
                             first_name VARCHAR(50) NOT NULL ,
                             last_name VARCHAR(50) NOT NULL ,
                             patronymic_name VARCHAR(50),
                             birth_date DATE,
                             birth_place VARCHAR(50),
                             pass_number VARCHAR(10) NOT NULL
);

create sequence if not exists account_id_seq start 1;

CREATE TABLE accounts (
                          account_id INTEGER NOT NULL DEFAULT nextval('account_id_seq') PRIMARY KEY,
                          account_number VARCHAR(16) NOT NULL UNIQUE,
                          account_holder INTEGER NOT NULL references cardholders(cardholder_id),
                          is_blocked BOOLEAN
);

create sequence if not exists card_id_seq start 1;

CREATE TABLE business_cards (
                             card_id INTEGER not null default nextval('card_id_seq') primary key,
                             card_number VARCHAR(16) UNIQUE NOT NULL,
                             cardholder_id INTEGER references cardholders(cardholder_id),
                             expiration_date DATE,
                             account_id INTEGER NOT NULL references accounts(account_id),
                             is_blocked BOOLEAN
);



