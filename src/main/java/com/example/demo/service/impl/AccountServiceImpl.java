package com.example.demo.service.impl;

import com.example.demo.builder.AccountNumberBuilder;
import com.example.demo.dao.AccountMapper;
import com.example.demo.dao.CardholderMapper;
import com.example.demo.dto.AccountDto;
import com.example.demo.entity.Account;
import com.example.demo.exceptionHandling.exception.AccountNotFoundException;
import com.example.demo.exceptionHandling.exception.CardholderNotFoundException;
import com.example.demo.service.api.AccountService;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AccountServiceImpl implements AccountService {


    AccountNumberBuilder accountNumberBuilder;
    ModelMapper modelMapper;
    AccountMapper accountMapper;
    CardholderMapper cardholderMapper;

    @Autowired
    public AccountServiceImpl(AccountNumberBuilder accountNumberBuilder, ModelMapper modelMapper,
                              AccountMapper accountMapper, CardholderMapper cardholderMapper) {
        this.accountNumberBuilder = accountNumberBuilder;
        this.modelMapper = modelMapper;
        this.accountMapper = accountMapper;
        this.cardholderMapper = cardholderMapper;
    }

    @Override
    public AccountDto addAccount(AccountDto accountDto) {
        Account account = modelMapper.map(accountDto, Account.class);
        account.setAccountNumber(accountNumberBuilder.createAccountNumber());
        accountMapper.addAccount(account);
        return modelMapper.map(account, AccountDto.class);
    }

    @Override
    public AccountDto updateAccount(AccountDto accountDto) {
        Account account = modelMapper.map(accountDto, Account.class);
        accountMapper.updateAccount(account);
        return modelMapper.map(account, AccountDto.class);
    }

    @Override
    public AccountDto unblockAccountById(int id) {
        accountMapper.getAccountById(id).orElseThrow(
                () -> new AccountNotFoundException("There is no account with id = " + id)
        );
        accountMapper.unblockAccountById(id);
        return getAccountById(id);
    }

    @Override
    public AccountDto blockAccountById(int id) {
        accountMapper.getAccountById(id).orElseThrow(
                () -> new AccountNotFoundException("There is no account with id = " + id)
        );
        accountMapper.blockAccountById(id);
        return getAccountById(id);
    }

    @Override
    public List<AccountDto> getAllAccountsByAccountHolderId(int id) {
        cardholderMapper.getCardholderById(id).orElseThrow(
                () -> new CardholderNotFoundException("There is no cardholder with id = " + id)
        );
        List<Account> entities = accountMapper.getAllAccountsByAccountHolderId(id);
        return modelMapper.map(entities, TypeToken.of(List.class).getType());
    }

    @Override
    public AccountDto getAccountById(int id) {
        Account account = accountMapper.getAccountById(id).orElseThrow(
                () -> new AccountNotFoundException("There is no account with id = " + id)
        );
        return modelMapper.map(account, AccountDto.class);
    }


}
