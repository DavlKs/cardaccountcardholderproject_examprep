package com.example.demo;

import com.example.demo.server.GRPCServer;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;

@SpringBootApplication
public class DemoApplication {

    public static void main(String[] args) throws IOException, InterruptedException {


        SpringApplication.run(DemoApplication.class, args);

        Server server = ServerBuilder
                .forPort(8082)
                .addService(new GRPCServer())
                .build();

        server.start();

        server.awaitTermination();

    }




}
