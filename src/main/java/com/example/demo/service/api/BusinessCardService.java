package com.example.demo.service.api;

import com.example.demo.dto.BusinessCardDto;
import java.util.List;


public interface BusinessCardService {


    BusinessCardDto addBusinessCard(BusinessCardDto businessCardDto);

    BusinessCardDto updateBusinessCard(BusinessCardDto businessCardDto);

    BusinessCardDto blockBusinessCardById(int id);

    BusinessCardDto unblockBusinessCardById(int id);

    List<BusinessCardDto> getAllCardsByCardholderId(int id);

    List<BusinessCardDto> getAllCardsByAccountId(int id);

    BusinessCardDto getBusinessCardById(int id);

}
