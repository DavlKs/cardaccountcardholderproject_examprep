package com.example.demo.controller;

import com.example.demo.dto.BusinessCardDto;
import com.example.demo.exceptionHandling.dto.BusinessCardNotFoundDto;
import com.example.demo.exceptionHandling.exception.BusinessCardNotFoundException;
import com.example.demo.service.api.BusinessCardService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@Tag(name = "Бизнес карта", description = "API для бизнес карт")
@RestController
@RequestMapping("/cards")
public class BusinessCardController {

    private BusinessCardService businessCardService;

    @Autowired
    public BusinessCardController(BusinessCardService businessCardService) {
        this.businessCardService = businessCardService;
    }

    @PostMapping
    @Operation(summary = "Добавление новой карты")
    public BusinessCardDto addBusinessCard(@RequestBody BusinessCardDto businessCardDto) {
        return businessCardService.addBusinessCard(businessCardDto);
    }

    @PutMapping
    @Operation(summary = "Обновление существующей карты")
    public BusinessCardDto updateBusinessCard(@RequestBody BusinessCardDto businessCardDto) {
        return businessCardService.updateBusinessCard(businessCardDto);
    }

    @PutMapping("/block/{id}")
    @Operation(summary = "Блокирование карты")
    public BusinessCardDto blockBusinessCardById(@PathVariable int id) {
        return businessCardService.blockBusinessCardById(id);
    }

    @PutMapping("/unblock/{id}")
    @Operation(summary = "Разблокирование карты")
    public BusinessCardDto unblockBusinessCardById(@PathVariable int id) {
        return businessCardService.unblockBusinessCardById(id);
    }

    @GetMapping("/cardholder/{id}")
    @Operation(summary = "Получение всех карт по идентификатору держателя карт")
    public List<BusinessCardDto> getAllCardsByCardholderId(@PathVariable int id) {
        return businessCardService.getAllCardsByCardholderId(id);
    }

    @GetMapping("/account/{id}")
    @Operation(summary = "Получение всех карт по идентификатору счета")
    public List<BusinessCardDto> getAllCardsByAccountId(@PathVariable int id) {
        return businessCardService.getAllCardsByAccountId(id);
    }

    @ExceptionHandler
    public ResponseEntity<BusinessCardNotFoundDto> handleException (BusinessCardNotFoundException exception) {
        BusinessCardNotFoundDto card = new BusinessCardNotFoundDto();
        card.setInfo(exception.getMessage());
        return new ResponseEntity<>(card, HttpStatus.NOT_FOUND);
    }
}
