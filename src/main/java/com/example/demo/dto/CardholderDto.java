package com.example.demo.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.time.LocalDate;

@Data
@Schema(title = "Модель данных держателя карты")
public class CardholderDto {

    @Schema(title = "Идентификатор держателя карты")
    int cardholderId;
    @Schema(title = "Имя держателя карты")
    String firstName;
    @Schema(title = "Фамилия держателя карты")
    String lastName;
    @Schema(title = "Отчество держателя карты")
    String patronymicName;
    @Schema(title = "Дата рождения")
    LocalDate birthDate;
    @Schema(title = "Место рождения")
    String birthPlace;
    @Schema(title = "Номер паспорта")
    String passNumber;

}
