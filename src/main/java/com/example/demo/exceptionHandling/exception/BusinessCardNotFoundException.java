package com.example.demo.exceptionHandling.exception;

public class BusinessCardNotFoundException  extends RuntimeException {

    public BusinessCardNotFoundException(String message) {
        super(message);
    }
}
