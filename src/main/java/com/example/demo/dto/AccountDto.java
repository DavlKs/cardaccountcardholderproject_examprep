package com.example.demo.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
@Schema(title = "Модель данных счета")
public class AccountDto {

    @Schema(title = "Идентификатор счета")
    int accountId;
    @Schema(title = "Номер счета")
    String accountNumber;
    @Schema(title = "Идентификатор держателя счета")
    int accountHolderId;
    @Schema(title = "Счет является заблокированным")
    boolean isBlocked;

}
