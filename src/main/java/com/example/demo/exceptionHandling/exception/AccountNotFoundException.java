package com.example.demo.exceptionHandling.exception;

public class AccountNotFoundException extends RuntimeException {


    public AccountNotFoundException(String message) {
        super(message);
    }
}
