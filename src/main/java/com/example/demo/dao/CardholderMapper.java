package com.example.demo.dao;

import com.example.demo.entity.Cardholder;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Mapper
@Repository
public interface CardholderMapper {

    @Select("select * from cardholders where cardholder_id = #{id}")
    Optional<Cardholder> getCardholderById(int id);

    @Insert("insert into cardholders values(#{cardholderId}, #{firstName}, #{lastName}, " +
            "#{patronymicName}, #{birthDate}, #{birthPlace}, #{passNumber})")
    @SelectKey(keyProperty = "cardholderId", before = true, resultType = Integer.class,
            statement = "select nextval('cardholder_id_seq')")
    void addCardholder(Cardholder cardHolder);

    @Delete("delete from cardholders where cardholder_id = #{id}")
    void deleteCardholder(int id);

    @Delete("delete from business_cards where cardholder_id = #{id}")
    void deleteCardholderCards(int id);

    @Delete("delete from accounts where account_holder = #{id}")
    void deleteCardholderAccounts(int id);

    @Update("update cardholders set first_name = #{firstName}, last_name = #{lastName}, " +
            "patronymic_name = #{patronymicName}, birth_date = #{birthDate}, birth_place = #{birthPlace}," +
            "pass_number = #{passNumber} where cardholder_id = #{cardholderId}")
    void updateCardholder(Cardholder cardholder);


}
