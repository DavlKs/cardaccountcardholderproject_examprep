package com.example.demo.security.config;

import com.example.demo.security.service.impl.AppUserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    private final PasswordEncoder passwordEncoder;
    private final AppUserServiceImpl userService;

    @Autowired
    public SecurityConfiguration(PasswordEncoder passwordEncoder, AppUserServiceImpl userService) {
        this.passwordEncoder = passwordEncoder;
        this.userService = userService;
    }

    @Override
    public void configure(WebSecurity web) {
        web.ignoring().antMatchers("/v3/api-docs/**", "/swagger-ui.html", "/swagger-ui/**");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeRequests()
                .antMatchers(HttpMethod.POST, "/api/login").permitAll()
                .antMatchers(HttpMethod.POST, "/api/register").permitAll()
                .antMatchers(HttpMethod.POST, "/accounts/**").hasRole("ACCOUNTANT")
                .antMatchers(HttpMethod.PUT, "/accounts/**").hasRole("ACCOUNTANT")
                .antMatchers(HttpMethod.DELETE, "/accounts/**").hasRole("ACCOUNTANT")
                .antMatchers(HttpMethod.POST, "/cards/**").hasAnyRole("USER", "ADMIN")
                .antMatchers(HttpMethod.DELETE, "/cards/**").hasAnyRole("USER", "ADMIN")
                .antMatchers(HttpMethod.PUT, "/cards/**").hasAnyRole("USER", "ADMIN")
                .antMatchers(HttpMethod.GET, "/cards/{id}").hasAnyRole("USER", "ADMIN")
                .antMatchers(HttpMethod.GET, "/cards/**").hasAnyRole("ADMIN")
                .antMatchers(HttpMethod.GET, "/cardholders/{id}").hasAnyRole("USER", "ADMIN")
                .antMatchers(HttpMethod.POST, "/cardholders/**").hasRole("ADMIN")
                .antMatchers(HttpMethod.PUT, "/cardholders/**").hasRole("ADMIN")
                .antMatchers(HttpMethod.DELETE, "/cardholders/{id}").hasRole("ADMIN");
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }


    @Autowired
    public void configureAuthentication(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        authenticationManagerBuilder
                .userDetailsService(userService)
                .passwordEncoder(passwordEncoder);
    }


}
