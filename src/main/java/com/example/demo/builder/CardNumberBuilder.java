package com.example.demo.builder;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Random;

/**
 * Класс создает номер бизнес карты или счета, используя алгоритм Луна
 */

@Component
public class CardNumberBuilder {

    @Bean
    public String createNumber() {

        Random random = new Random();
        int sum = 0;
        StringBuilder numberWithoutCheckSum = new StringBuilder("400000");
        int x = 0;
        while (x < 9) {
            numberWithoutCheckSum.append(random.nextInt(9));
            x++;
        }
        String[] numberAsString = numberWithoutCheckSum.toString().split("");

        int[] arrayOfCardNumbers = new int[numberAsString.length];
        for (int i = 0; i < arrayOfCardNumbers.length; i++) {
            int number  = Integer.parseInt(numberAsString[i]);
            if (i % 2 == 1) {
                arrayOfCardNumbers[i] = number;
            } else {
                if ((number * 2) > 9) {
                    arrayOfCardNumbers[i] = number * 2 - 9;
                } else {
                    arrayOfCardNumbers[i] = number * 2;
                }
            }
        }

        sum = Arrays.stream(arrayOfCardNumbers).reduce((accum, element) -> accum + element).getAsInt();

        int checksum = (sum % 10 == 0 ? 0 : 10 - (sum % 10));

        return numberWithoutCheckSum.append(checksum).toString();

    }
}
