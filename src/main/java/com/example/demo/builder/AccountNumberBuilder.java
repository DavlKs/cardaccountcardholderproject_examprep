package com.example.demo.builder;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.Random;

@Component
public class AccountNumberBuilder {

    @Bean
    public String createAccountNumber() {

        Random random = new Random();

        StringBuilder accountNumber = new StringBuilder("4400");

        for (int i = 0; i < 16; i++) {
            accountNumber.append(random.nextInt(9));
        }

        return accountNumber.toString();
    }
}
