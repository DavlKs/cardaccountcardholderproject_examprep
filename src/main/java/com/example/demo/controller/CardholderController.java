package com.example.demo.controller;

import com.example.demo.dto.CardholderDto;
import com.example.demo.exceptionHandling.dto.AccountNotFoundDto;
import com.example.demo.exceptionHandling.dto.CardholderNotFoundDto;
import com.example.demo.exceptionHandling.exception.CardholderNotFoundException;
import com.example.demo.service.api.CardholderService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Tag(name = "Держатель бизнес карт", description = "API для держателей бизнес карт")
@RestController
@RequestMapping("/cardholders")
public class CardholderController {

    CardholderService cardholderService;

    @Autowired
    public CardholderController(CardholderService cardholderService) {
        this.cardholderService = cardholderService;
    }

    @GetMapping("/{id}")
    @Operation(summary = "Получение держателя карт по идентификатору")
    public CardholderDto getCardholderById(@PathVariable int id) {
        return cardholderService.getCardholderById(id);
    }

    @PostMapping
    @Operation(summary = "Добавление нового держателя карт")
    public CardholderDto addCardholder(@RequestBody CardholderDto cardholderDto) {
        return cardholderService.addCardholder(cardholderDto);
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Удаление держателя карт")
    public void deleteCardholder(@PathVariable int id) {
        cardholderService.deleteCardholder(id);
    }

    @PutMapping
    @Operation(summary = "Обновление держателя карт")
    public CardholderDto updateCardholder(@RequestBody CardholderDto cardholderDto) {
        return cardholderService.updateCardholder(cardholderDto);
    }

    @ExceptionHandler
    public ResponseEntity<CardholderNotFoundDto> handleException (CardholderNotFoundException exception) {
        CardholderNotFoundDto cardholder = new CardholderNotFoundDto();
        cardholder.setInfo(exception.getMessage());
        return new ResponseEntity<>(cardholder, HttpStatus.NOT_FOUND);
    }
}
