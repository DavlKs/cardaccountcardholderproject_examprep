package com.example.demo.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.time.LocalDate;

@Data
@Schema(title = "Модель данных бизнес карты")
public class BusinessCardDto {

    @Schema(title = "Идентификатор карты")
    int cardId;
    @Schema(title = "Номер карты")
    String cardNumber;
    @Schema(title = "Идентификатор держателя карты")
    int cardholderId;
    @Schema(title = "Дата окончания срока действия карты")
    LocalDate expirationDate;
    @Schema(title = "Идентификатор счета, к которому привязана карта")
    int accountId;
    @Schema(title = "карта является заблокированной")
    boolean isBlocked;
}
