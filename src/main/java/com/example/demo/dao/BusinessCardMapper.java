package com.example.demo.dao;

import com.example.demo.entity.BusinessCard;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Mapper
@Repository
public interface BusinessCardMapper {

    @Insert("insert into business_cards values (#{cardId}, #{cardNumber}, #{cardholderId}, " +
            "#{expirationDate}, #{accountId}, #{isBlocked})")
    @SelectKey(keyProperty = "cardId", before = true, resultType = Integer.class,
            statement = "select nextval('card_id_seq')")
    void addBusinessCard(BusinessCard businessCard);

    @Update("update business_cards set card_number = #{cardNumber}, cardholder_id = #{cardholderId}, " +
            "expiration_date = #{expirationDate}, account_id = #{accountId}, is_blocked = #{isBlocked} " +
            "where card_id = #{cardId}")
    void updateBusinessCard(BusinessCard businessCard);

    @Update("update business_cards set is_blocked = true where card_id = #{id}")
    void blockBusinessCardById(int id);

    @Update("update business_cards set is_blocked = false where card_id = #{id}")
    void unblockBusinessCardById(int id);

    @Select("select * from business_cards where cardholder_id = #{id}")
    List<BusinessCard> getAllCardsByCardholderId(int id);

    @Select("select * from business_cards where account_id = #{id}")
    List<BusinessCard> getAllCardsByAccountId(int id);

    @Select("select * from business_cards where card_id = #{id}")
    Optional<BusinessCard> getBusinessCardById(int id);
}
