package com.example.demo.service.impl;

import com.example.demo.builder.CardNumberBuilder;
import com.example.demo.dao.AccountMapper;
import com.example.demo.dao.BusinessCardMapper;
import com.example.demo.dao.CardholderMapper;
import com.example.demo.dto.BusinessCardDto;
import com.example.demo.entity.BusinessCard;
import com.example.demo.exceptionHandling.exception.AccountNotFoundException;
import com.example.demo.exceptionHandling.exception.BusinessCardNotFoundException;
import com.example.demo.exceptionHandling.exception.CardholderNotFoundException;
import com.example.demo.service.api.BusinessCardService;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BusinessCardServiceImpl implements BusinessCardService {

    ModelMapper modelMapper;
    CardNumberBuilder cardNumberBuilder;
    BusinessCardMapper businessCardMapper;
    CardholderMapper cardholderMapper;
    AccountMapper accountMapper;

    @Autowired()
    public BusinessCardServiceImpl(ModelMapper modelMapper, CardNumberBuilder cardNumberBuilder,
                                   BusinessCardMapper businessCardMapper, CardholderMapper cardholderMapper,
                                   AccountMapper accountMapper) {
        this.modelMapper = modelMapper;
        this.cardNumberBuilder = cardNumberBuilder;
        this.businessCardMapper = businessCardMapper;
        this.cardholderMapper = cardholderMapper;
        this.accountMapper = accountMapper;
    }

    @Override
    public BusinessCardDto addBusinessCard(BusinessCardDto businessCardDto) {
        BusinessCard businessCard = modelMapper.map(businessCardDto, BusinessCard.class);
        businessCard.setCardNumber(cardNumberBuilder.createNumber());
        businessCardMapper.addBusinessCard(businessCard);
        return modelMapper.map(businessCard, BusinessCardDto.class);
    }

    @Override
    public BusinessCardDto updateBusinessCard(BusinessCardDto businessCardDto) {
        BusinessCard businessCard = modelMapper.map(businessCardDto, BusinessCard.class);
        businessCardMapper.updateBusinessCard(businessCard);
        return modelMapper.map(businessCard, BusinessCardDto.class);
    }

    @Override
    public BusinessCardDto blockBusinessCardById(int id) {
        businessCardMapper.getBusinessCardById(id).orElseThrow(
                () -> new ArithmeticException("There is no business card with id = " + id)
        );
        businessCardMapper.blockBusinessCardById(id);
        return getBusinessCardById(id);
    }

    @Override
    public BusinessCardDto unblockBusinessCardById(int id) {
        businessCardMapper.getBusinessCardById(id).orElseThrow(
                () -> new ArithmeticException("There is no business card with id = " + id)
        );
        businessCardMapper.unblockBusinessCardById(id);
        return getBusinessCardById(id);
    }

    @Override
    public List<BusinessCardDto> getAllCardsByCardholderId(int id) {
        cardholderMapper.getCardholderById(id).orElseThrow(
                () -> new CardholderNotFoundException("There is no cardholder with id = " + id)
        );
        List<BusinessCard> entities = businessCardMapper.getAllCardsByCardholderId(id);
        return modelMapper.map(entities, TypeToken.of(List.class).getType());
    }

    @Override
    public List<BusinessCardDto> getAllCardsByAccountId(int id) {
        accountMapper.getAccountById(id).orElseThrow(
                () -> new AccountNotFoundException("There is no account with id = " + id)
        );
        List<BusinessCard> entities = businessCardMapper.getAllCardsByAccountId(id);
        return modelMapper.map(entities, TypeToken.of(List.class).getType());
    }

    @Override
    public BusinessCardDto getBusinessCardById(int id) {
        BusinessCard businessCard = businessCardMapper.getBusinessCardById(id).orElseThrow(
                () -> new BusinessCardNotFoundException("There is no business card with id = " + id)
        );
        return modelMapper.map(businessCard, BusinessCardDto.class);
    }
}
