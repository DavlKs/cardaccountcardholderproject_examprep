package com.example.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BusinessCard {

    int cardId;
    String cardNumber;
    int cardholderId;
    LocalDate expirationDate;
    int accountId;
    boolean isBlocked;

}
